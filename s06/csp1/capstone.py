from abc import ABC, abstractclassmethod

class Person(ABC):

	@abstractclassmethod
	def getfullname(self):
		pass

	def addrequest(self):
		return f'Request has been added'
		pass

	def checkrequest(self):
		pass

	def adduser(self):
		pass

class Employee(Person):

	def __init__ (self, firstname, lastname,email,department):
		
		super().__init__()
		self._firstname= firstname
		self._lastname= lastname
		self._email= email
		self._department= department

	#Setters

	def set_firstname(self, firstname):
		self._firstname= firstname

	def set_lastname(self, lastname):
		self._lastname= lastname

	def set_email(self, email):
		self._email= email

	def set_department(self, department):
		self._department= department

	#Getters

	def get_firstname(self):
		print(f'Employee First Name :{self._firstname}')

	def get_lastname(self):
		print(f'Employee Last Name  : {self._lastname}')

	def get_email(self):
		print(f'The Employee Email is {self._email}')

	def get_department(self):
		print(f'The Employee Department is {self._department}')

	#Abstract Methods

	def getfullname(self):
		return f'{self._firstname} {self._lastname}'

	def checkrequest(self):
		pass
	
	def adduser(self):
		pass

	def login(self):
		return f'{self._email} has logged in'

	def logout(self):
		return f'{self._email} has logged out'

class TeamLead(Person):

	def __init__ (self, firstname, lastname,email,department):
		
		super().__init__()
		self._firstname= firstname
		self._lastname= lastname
		self._email= email
		self._department= department
		self._members = set()

	def set_firstname(self, firstname):
		self._firstname= firstname

	def set_lastname(self, lastname):
		self._lastname= lastname

	def set_email(self, email):
		self._email= email

	def set_department(self, department):
		self._department= department

	def get_firstname(self):
		print(f'TeamLead First Name :{self._firstname}')

	def get_lastname(self):
		print(f'TeamLead Last Name  : {self._lastname}')

	def get_email(self):
		print(f'The TeamLead Email is {self._email}')

	def get_department(self):
		print(f'The TeamLead Department is {self._department}')

	def getfullname(self):
		return f'{self._firstname} {self._lastname}'

	def checkrequest(self):
		pass
	
	def adduser(self):
		pass

	def login(self):
		print(f'{self._email} has logged in')

	def login(self):
		return f'{self._email} has logged in'

	def logout(self):
		return f'{self._email} has logged out'

	def addmember(self,Employee):
		self._members.add(Employee)
	
	def get_members(self):
		return self._members

class Admin(Person):

	def __init__ (self, firstname, lastname,email,department):
		
		super().__init__()
		self._firstname= firstname
		self._lastname= lastname
		self._email= email
		self._department= department

	def set_firstname(self, firstname):
		self._firstname= firstname

	def set_lastname(self, lastname):
		self._lastname= lastname

	def set_email(self, email):
		self._email= email

	def set_department(self, department):
		self._department= department

	def get_firstname(self):
		print(f'Admin First Name :{self._firstname}')

	def get_lastname(self):
		print(f'Admin Last Name  : {self._lastname}')

	def get_email(self):
		print(f'The Admin Email is {self._email}')

	def get_department(self):
		print(f'The Admin Department is {self._department}')

	def getfullname(self):
		return f'{self._firstname} {self._lastname}'

	def checkrequest(self):
		pass
	
	def adduser(self):
		return f'User has been added'


	def login(self):
		return f'{self._email} has logged in'

	def logout(self):
		return f'{self._email} has logged out'

	def addmember():
		print('New User Added')


class Request():
	def __init__(self, name, requester, daterequested, status):
		super().__init__()
		self._name = name
		self._requester = requester
		self._daterequested = daterequested
		self._status = status

	def updateRequest(self):
		pass

	def closeRequest(self):
		return f'Request {self._name} has been closed'

	def cancelRequest(self):
		pass

	def set_status(self,status):
		self._status = status



employee1=Employee("John", "Doe", "djohn@email.com", "Marketing")
employee2=Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3=Employee("Robert", "Patterson", "probert@email.com", "Sales")
employee4=Employee("Brandon", "Smith", "sbrandon@email.com", "Sales")
admin1=Admin("Monika", "Justin", "jmonika@email.com", "Marketing")
teamlead1=TeamLead("Michael","Specter", "smichael@email.com", "Sales")
req1=Request("New Hire Orientation", teamlead1, "27-Jul-2021","Processing")
req2=Request("Laptop Repair", employee1, "1-Jul-2021","Processing")

assert employee1.getfullname() == "John Doe", "Full name should be John Doe"
assert admin1.getfullname() == "Monika Justin", "Full name should be Monika Justin"
assert teamlead1.getfullname() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addrequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamlead1.addmember(employee3)
teamlead1.addmember(employee4)
for indiv_emp in teamlead1.get_members():
    print(indiv_emp.getfullname())

assert admin1.adduser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
		